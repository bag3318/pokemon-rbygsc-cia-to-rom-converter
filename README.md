[comment]: # (BEGIN README.md)

# Pokemon RBYGSC CIA to ROM Converter

## About

This is a conversion tool that will take a Pokemon Red/Blue/Yellow/Gold/Silver/Crystal `.cia` file dumped from a CFW 3DS and turn it into the exact ROM that would be on the cartridge of that game.

The resulting ROM can be played computer emulators and Nintendo consoles alike.

## Download

Download link: [https://bit.ly/2VlrPs7](https://bit.ly/2VlrPs7)

## How to use

### Windows

It\'s simple! 

Drag your Pokemon Red/Blue/Yellow/Gold/Silver/Crystal `.cia` file onto the `converter.exe` file.

Done!

### OSX

On a Mac, you just have to follow a few extra steps to this app up and running.

1. Download & install [Mono for Mac](https://www.mono-project.com/docs/getting-started/install/mac/)
2. Open Terminal
3. Type `mono ` (make sure to add a <kbd>space</kbd> after `mono`)
4. Drag `converter.exe` file onto terminal. 
5. Insert a <kbd>space</kbd>
6. Drag drag your Pokemon Red/Blue/Yellow/Gold/Silver/Crystal `.cia` file onto the terminal
7. Hit <kbd>return</kbd>

## Build

Building this solution requires Visual Studio Community 2019. 

Windows Download Link: [https://visualstudio.microsoft.com/vs/](https://visualstudio.microsoft.com/vs/)

> Hover over **Download Visual Studio** and click **Community 2019**.

OSX/Mac Download Link: [https://visualstudio.microsoft.com/vs/mac/](https://visualstudio.microsoft.com/vs/mac/)

You must install the following packages with the Visual Studio installer:
* .NET Desktop Development (Windows)
* .NET Core Cross Platform Development (Windows, Mac)

Then open the `.sln` file with Visual Studio

On Windows, press <kbd>Ctrl</kbd> + <kbd>Shift</kbd> + <kbd>B</kbd>

On Mac, press <kbd>Cmd</kbd> + <kbd>Shift</kbd> + <kbd>B</kbd>

The output file(s) will be in the `{solution_dir}/{project_dir}/bin/debug` folder; if you switched the build configuration to release, it will be in the `{solution_dir}/{project_dir}/bin/release` folder.

________

> _**Enjoy!**_

[comment]: # "END README.md"
