﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace PKMN_RBYGSC_CIA_to_ROM_Converter
{
    /// <summary>
    /// The main console app class.
    /// </summary>
    internal class Program
    {
        /// <summary>
        /// The eacute character used in the word "Pok[eacute]mon".
        /// </summary>
        private static readonly char _eacute = '\xE9';

        /// <summary>
        /// The Pokemon string with the eacute character.
        /// </summary>
        private static readonly string _pokemon = $"Pok{_eacute}mon";

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// <param name="args">Command line arguments. Argument 1 used for input file.</param>
        private static void Main(string[] args)
        {
            Console.WriteLine("|=====================================|");
            Console.WriteLine("|                                     |");
            Console.WriteLine("| CIA to {0} RBYGSC ROM Converter |", _pokemon);
            Console.WriteLine("|                                     |");
            Console.WriteLine("|=====================================|");
            Console.Write("\r\n");
            if (args.Length == 0)
            {
                Console.WriteLine("Error: no file data to read.");
                Console.WriteLine("Exit then drag RBYGSC .cia file onto this application file.");
                Console.Write("Press enter to exit...");
                Console.ReadKey();
                Environment.Exit(0);
            }
            List<byte> fileBytes = File.ReadAllBytes(args[0]).ToList();
            string outputFileName = string.Empty;
            switch (fileBytes.Count)
            {
                case 0x9D1400:
                    fileBytes.RemoveRange(0x00, 0x411FB0);
                    fileBytes.RemoveRange(0x100000, 0x4BF450);
                    if (fileBytes[0x13C] == 0x52)
                    {
                        Console.WriteLine("{0} Red detected...", _pokemon);
                        File.WriteAllBytes("POKEMON RED.gb", fileBytes.ToArray());
                        outputFileName = "POKEMON RED.gb";
                    }
                    else if (fileBytes[0x13C] == 0x42)
                    {
                        Console.WriteLine("{0} Blue detected...", _pokemon);
                        File.WriteAllBytes("POKEMON BLUE.gb", fileBytes.ToArray());
                        outputFileName = "POKEMON BLUE.gb";
                    }
                    else
                    {
                        Console.WriteLine("Error: Invalid {0} Red/Blue cia image.", _pokemon);
                        Console.Write("Press enter to exit...");
                        Console.ReadKey();
                        Environment.Exit(0);
                    }
                    break;
                case 0x9D5400:
                    fileBytes.RemoveRange(0x00, 0x4155B0);
                    fileBytes.RemoveRange(0x100000, 0x4BFE50);
                    if (fileBytes[0x13C] == 0x59)
                    {
                        Console.WriteLine("{0} Yellow detected...", _pokemon);
                        File.WriteAllBytes("POKEMON YELLOW.gb", fileBytes.ToArray());
                        outputFileName = "POKEMON YELLOW.gb";
                    }
                    else
                    {
                        Console.WriteLine("Error: Invalid {0} Yellow cia image.", _pokemon);
                        Console.Write("Press enter to exit...");
                        Console.ReadKey();
                        Environment.Exit(0);
                    }
                    break;
                case 0xAF1400:
                    fileBytes.RemoveRange(0x00, 0x475070);
                    fileBytes.RemoveRange(0x200000, 0x47C390);
                    if (fileBytes[0x13C] == 0x47)
                    {
                        Console.WriteLine("{0} Gold detected...", _pokemon);
                        File.WriteAllBytes("POKEMON_GLDAAUE.gbc", fileBytes.ToArray());
                        outputFileName = "POKEMON_GLDAAUE.gbc";
                    }
                    else if (fileBytes[0x13C] == 0x53)
                    {
                        Console.WriteLine("{0} Silver detected...", _pokemon);
                        File.WriteAllBytes("POKEMON_SLVAAXE.gbc", fileBytes.ToArray());
                        outputFileName = "POKEMON_SLVAAXE.gbc";
                    }
                    else
                    {
                        Console.WriteLine("Error: Invalid {0} Gold/Silver cia image.", _pokemon);
                        Console.Write("Press enter to exit...");
                        Console.ReadKey();
                        Environment.Exit(0);
                    }
                    break;
                case 0xB26400:
                    fileBytes.RemoveRange(0x00, 0x4A2F50);
                    fileBytes.RemoveRange(0x200000, 0x4834B0);
                    if (fileBytes[0x137] == 0x43)
                    {
                        Console.WriteLine("{0} Crystal detected...", _pokemon);
                        File.WriteAllBytes("PM_CRYSTAL_BYTE.gbc", fileBytes.ToArray());
                        outputFileName = "PM_CRYSTAL_BYTE.gbc";
                    }
                    else
                    {
                        Console.WriteLine("Error: Invalid {0} Crystal cia image.", _pokemon);
                        Console.Write("Press enter to exit...");
                        Console.ReadKey();
                        Environment.Exit(0);
                    }
                    break;
                default:
                    Console.WriteLine("Error: file not a valid RBYGSC .cia file.");
                    Console.Write("Press enter to exit...");
                    Console.ReadKey();
                    Environment.Exit(0);
                    break;
            }
            Console.Write("\r\n");
            Console.WriteLine("Output file path:\r\n{0}{1}{2}", Directory.GetCurrentDirectory(), Regex.Match(Environment.OSVersion.ToString(), @"\b(Windows)\b").Success ? @"\" : "/", outputFileName);
            Console.Write("\r\n");
            Console.Write("Done!");
            Console.ReadKey();
        }
    }
}
